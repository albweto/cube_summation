using System.Collections.Generic;

namespace cube_summation.Models.Interface
{

    public interface IOperacion
    {
        void EjecutarOperacion(List<string> informacionOperacion, Matriz matriz, List<string> resultadoOperacion);
    }

}