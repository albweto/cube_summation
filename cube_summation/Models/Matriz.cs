using System;

namespace cube_summation.Models
{

    public class Matriz
    {
        public int tamaño;

        public int[,,] contenidoMatriz;

        public Matriz(int tamaño)
        {
            this.tamaño = tamaño;
            ConstruirMatriz();

        }

        private void ConstruirMatriz()
        {
            contenidoMatriz = new int[tamaño, tamaño, tamaño];
        }
    }

}