using System;

namespace cube_summation.Models
{

    public class InformacionProceso
    {
        public string DatosEntrada { get; set; }

        public string DatosSalida { get; set; }
    }
}