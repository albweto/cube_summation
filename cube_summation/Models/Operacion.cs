using System.Collections.Generic;
using cube_summation.Models.Interface;


namespace cube_summation.Models
{
    public class Operacion
    {
        private IOperacion operacion;

        public Operacion()
        {

        }

        private void AsignarOperacion(IOperacion operacion)
        {
            this.operacion = operacion;
        }

        public void SeleccionarOperacion(List<string> informacionOperacion)
        {

            switch (informacionOperacion[0])
            {
                case "UPDATE":
                    AsignarOperacion(new Update());
                    break;

                case "QUERY":
                    AsignarOperacion(new Query());
                    break;
            }
        }


        public void EjecutarOperacion(List<string> informacionOperacion, Matriz matriz, List<string> resultadoOperacion)
        {
            this.operacion.EjecutarOperacion(informacionOperacion, matriz, resultadoOperacion);
        }

    }
}